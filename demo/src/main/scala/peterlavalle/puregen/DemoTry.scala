package peterlavalle.puregen

import java.io.File

import S3.S3
import org.graalvm.polyglot.Context

object DemoTry extends APureGenApp(new File("demo").AbsoluteFile) with TSpagoApp {

	override def s3(context: Context, pedal: BuiltIn.Hook, run: Runnable): Unit = {

		// create our systems and stuff
		val s3 = new S3(context, pedal, run)
			with TrySphinx

			with TheAudio
			with TheGCASR
			with TheScenario

			with TryMary

		// turn them on
		s3.apply()
	}
}
