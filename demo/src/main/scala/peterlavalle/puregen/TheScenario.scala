package peterlavalle.puregen

import java.util

import S3.Scenario
import peterlavalle.LockOnGet

trait TheScenario extends Scenario.D {

	// time stamp that the simulation started
	private lazy val start: Long = System.currentTimeMillis()

	// to get consistent timestamps we sample the age ONCE at the start of the cycle
	private var age: Double = -1

	private val buffered = new util.HashMap[String, util.LinkedList[String]]()

	private val tool: LockOnGet[(String, Tool.Signal[String])] = LockOnGet()

	before {

		// the only thing that we need-need to do is perform the time sample
		age = (System.currentTimeMillis() - start) * 0.001
	}

	override protected def S3_Scenario_openAge(): () => Double = {
		// just read the age - don't care
		() => age
	}

	override protected def S3_Scenario_openLogColumn(name: String): String => Unit = {

		// avoid name collisions
		require(!buffered.containsKey(name))

		// create the output thing
		tool += (name -> hook.toolRequest.log(name))

		// add a list to the list of lists of lists we've added
		buffered.put(name, new util.LinkedList[String]())

		// here's the callback we'll use to send stuff out
		(_: String)
			.split("[\r \t]*\n")
			.foreach(buffered(name).add)
	}

	//
	// this all happens after the
	follow {

		// only bother if there are any loggers
		if (buffered.nonEmpty) {

			//
			// pass all messages to the tooling
			tool(
				(_: Set[(String, Tool.Signal[String])])
					.toList
					.foreach {
						case (key, signal) =>
							signal {
								buffered(key)
									.foldLeft("")((_: String) + (_: String) + "\n")
							}
					}
			)

			//
			// build a big commandline wall of text
			val out: String =
			buffered.foldLeft("@ " + age) {
				case (left, (key, lines)) =>
					val list: List[String] = lines.toList
					lines.clear()
					list.foldLeft(left + "\n\t[" + key + "]")((_: String) + "\n\t\t" + (_: String))
			}

			//
			// write the text ... after flushing
			System.err.flush()
			System.out.flush()
			System.out.println(out)
		}
	}
}
