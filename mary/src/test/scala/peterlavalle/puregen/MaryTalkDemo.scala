package peterlavalle.puregen

import marytts.LocalMaryInterface
import marytts.modules.synthesis.{HMMSynthesizer, Voice}

object MaryTalkDemo extends App {
	//	val talk = MaryTalk()
	//
	//	talk("hello there")
	//
	//	talk("general kenobi")

	private val localMaryInterface = new LocalMaryInterface()

//	//	interface.getAvailableVoices.foreach(println)
//	//
//	//	interface.setVoice("cmu-slt-hsmm")
//
//	Voice.getAvailableVoices
//		.foreach(println)

	val List(voice) = Voice.getAvailableVoices.toList
//
//	println(
//	new Voice("cmu-slt-hsmm", new marytts.modules.synthesis.HMMSynthesizer())
//	)

	import marytts.modules.synthesis.{HMMSynthesizer, Voice}
	val forceVoice = new Voice(
		"cmu-slt-hsmm",
		localMaryInterface.getLocale, // lie about the locale?
		Voice.AF16000,
		new HMMSynthesizer(),
		Voice.FEMALE
	)

	Voice.registerVoice(forceVoice)

	println(Voice.AF16000)
	println(voice.dbAudioFormat())



}
