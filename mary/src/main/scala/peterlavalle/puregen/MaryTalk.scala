package peterlavalle.puregen

import java.io.FileWriter
import java.util.Date

import javax.sound.sampled.{AudioInputStream, AudioSystem, Clip, DataLine}
import javax.swing.JOptionPane
import marytts.LocalMaryInterface
import marytts.exceptions.{MaryConfigurationException, SynthesisException}
import marytts.modules.synthesis

object MaryTalk {

	/**
	 * very slow function - speaks something regardless of how long it takes to say
	 *
	 * ... so ... "printf()" but for TTS
	 */
	def apply(): String => Unit =
		try {
			val localMaryInterface: LocalMaryInterface =
				intercept[MaryConfigurationException]("mary-create.txt")
					.in {
						try {
							new LocalMaryInterface()
						} catch {
							case t: Throwable =>
								// workaround 1; just try it again
								System.err.println("MaryTTS failed, trying to 'workaround' by doing it again")

								// workaround 2; force the version number
								System.err.println("java.version=" + System.getProperty("java.version"))
								System.err.println("... but i'm changing it to 1.5 ...")
								System.setProperty("java.version", "1.5") // MaryTTS has a need

								new LocalMaryInterface()
						}
					}


			// workaround; try to manually create and set a voice
			intercept[MaryConfigurationException]("mary-set-voice.txt")
				.in {
					import marytts.modules.synthesis.{HMMSynthesizer, Voice}
					val forceVoice = new Voice(
						"cmu-slt-hsmm",
						localMaryInterface.getLocale, // lie about the locale?
						Voice.AF16000,
						new HMMSynthesizer(),
						Voice.FEMALE
					)

					Voice.registerVoice(forceVoice)
					localMaryInterface.setLocale(forceVoice.getLocale)
					localMaryInterface.setVoice(forceVoice.getName)
					System.err.println(
						localMaryInterface.getAvailableVoices(localMaryInterface.getLocale)
							.foldLeft(">>> localMaryInterface.getLocale = " + localMaryInterface.getLocale)(_ + "\n\t" + _) + "\n<<<"
					)
				}


			(text: String) =>
				if (text.trim.nonEmpty) {
					intercept[SynthesisException]("mary-errors.txt")
						.swallow {
							localMaryInterface.generateAudio(text).using {
								audioInputStream: AudioInputStream =>

									val info = new DataLine.Info(classOf[Clip], audioInputStream.getFormat)
									AudioSystem.getLine(info).asInstanceOf[Clip].using {
										clip: Clip =>
											clip.open(audioInputStream)
											clip.start()

											val microsecondLength: Long = clip.getMicrosecondLength

											Thread.sleep(microsecondLength / 1000L)

											while (clip.isRunning)
												Thread.sleep(10)

											clip.close()
											audioInputStream.close()
									}
							}
						}
				}
		} catch {
			case t: Throwable =>

				JOptionPane.showMessageDialog(null, "i can't do a real text-to-speech and will write to mary-said.txt")

				val writer = new FileWriter("mary-said.txt", true)
					.append(new Date().iso8061Long).append("\n")
				(line: String) => {
					writer
						.append(line).append("\n")
						.flush()
				}

		}
}
