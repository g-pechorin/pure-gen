package peterlavalle.puregen

object TLazyTalk {

	sealed trait StringLazyTalk[T] extends TLazyTalk[T, String, String] {
		/**
		 * breakdown a spoken message into work chunks
		 */
		override final protected def unpack(m: String): Seq[String] =
			m.split("\\s+")
				.toList
				.map(_.trim.toUpperCase())
	}

	def apply[T](cr: => T, sp: (T, String) => Unit, cl: T => Unit): TLazyTalk[T, String, String] =
		new StringLazyTalk[T] {
			override protected def create(): T = cr

			/**
			 * speak the next chunk of text
			 */
			override protected def speak(t: T, w: String): T = {
				sp(t, w)
				t
			}

			/**
			 * safely close the context
			 */
			override protected def close(t: T): Unit = cl(t)
		}
}

sealed trait TLazyTalk[T, M, W] extends AutoCloseable {

	/**
	 * create a new instance of the context thing
	 */
	protected def create(): T

	/**
	 * if "none" we're fresh and unopened
	 * if "some" we're happily running
	 * if "null" we've been closed
	 */
	private var open: Option[T] = None

	private lazy val thread: Thread =
		new Thread() {
			override def run(): Unit = {
				error("do ... something")
			}


		}

	/**
	 * send a message to be spoken
	 *
	 * @param m
	 */
	final def send(m: M, open: () => Unit, done: Boolean => Unit): Unit = {
		???
	}

	/**
	 * breakdown a spoken message into work chunks
	 */
	protected def unpack(m: M): Seq[W]

	/**
	 * speak the next chunk of text
	 */
	protected def speak(t: T, w: W): T

	/**
	 * safely close the context
	 */
	protected def close(t: T): Unit

	final def close(): Unit = {
		synchronized {
			if (expect(null != open, "looks like this was already closed")) {
				if (!expect(open.nonEmpty, "looks like this wasn't started")) {
					open = null
				} else {

					thread.join()
					require(null == open)
				}
			}
		}
	}
}
