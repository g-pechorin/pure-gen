

js tools that i tried out on the generated purescript

https://prepack.io/
- no; tried to play with an undefined

https://javascript-minifier.com/
- no; tried to getMember on a bool

https://www.minifier.org/
- no; seems to burn the PS Eff

https://jscompress.com/
- no; tried to getMember on a bool

https://babeljs.io/
- laid it out pretty
- seems to be fine
- looks like common structures could be extracted and (maybe) convert the JS back to jvm
	- ... or even C?

https://closure-compiler.apps
- when used the babel output and advanced
	- no; hit some snag with "q.Y"
- when used with the orignal and advanced
	- no; hit some snag with "q.Y"
- when used with the orignal and SIMPLE
	- no; seems to burn the PS Eff
