

On 2021-07-09 I finished the "nonode" changes which simplified the setup of this system.
The project now operates on amd64 based Windows, Linux and MacOS computers.<a id='f_link1' name='f_link1'/><sup>[1](#f_note1)</sup>
You need git<a id='f_link2' name='f_link2'/><sup>[2](#f_note2)</sup> and [JDK11+](https://adoptopenjdk.net/) for the software to work - you can check with the following commands;

```bash
$ git --version
$ javac --version
$ java --version
```

The git version is probably irelevant but Java has to be 11 or later because Oracle/GraalJS demands that.

With that in place you can clone (et al) the project and "test" it to be sure it's all running;

```bash
$ git clone -b default https://github.com/g-pechorin/pure-gen.git
$ cd pure-gen
$ java -jar sbt.bin/sbt-launch.jar test
```

To see it running, run `$ java -jar sbt.bin/sbt-launch.jar demo/run` and (after a wall of text) the "agent window" should appear like this;

![running-agent](doc/running-agent.png)

I have tested this on;

- Windows 7 (my Desktop)
- Ubuntu ??? (my Laptop)
- an old Mac Mini

[When you're ready - why not continue with the tutorial?](TUTORIAL.md)
