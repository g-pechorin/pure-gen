

> Hey me, clone/test like this; `git clone -b default https://gitlab.com/g-pechorin/pure-gen.git && cd pure-gen && java -jar sbt.bin/sbt-launch.jar test && java -jar sbt.bin/sbt-launch.jar demo/run`

This system demonstrates using [the pure functional programming language PureScript](https://www.purescript.org/) to ["script" (in the Unity3D sense)](https://docs.unity3d.com/Manual/ScriptingSection.html) the interactions between components to make an Interactive AI.
For the time being - there are only speech recognition and speech synthesis components.
This project is a "architectural" demonstration of the concept.

[Installation Instructions](INSTALL.md) provide a guide to ensuring that the system "installs" on your PC.

[A long-winded tutorial](TUTORIAL.md) is intended to walk a non-Functional-Programming developer through constructing a crude system with this.
