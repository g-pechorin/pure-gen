package pal

import peterlavalle.puregen.S3
import sbt.Keys._
import sbt.{AutoPlugin, _}

object PIDLPlugin extends AutoPlugin {

	// override def trigger = allRequirements

	object autoImport {

		// declare settings
		val idlRoots = settingKey[Seq[File]]("folders to scan for .pidl in")
		val idlTargetScala = settingKey[File]("where pidl should put .scala")
		val idlTargetScript = settingKey[File]("where pidl should put .purs and .js")

		val idlClassPath = taskKey[Seq[File]]("the classpath thingie that .pidl files will be looked up on")

		// declare the two tasks
		val idlGenerateScala: TaskKey[List[File]] = taskKey[List[File]]("generate some scala")
		val idlGenerateScript: TaskKey[List[File]] = taskKey[List[File]]("generate some pure script")


	}

	import autoImport._

	override lazy val projectSettings: Seq[Setting[_]] = Seq(

		// configure the settings
		idlRoots := {
			Seq((Compile / sourceDirectory).value / "pidl")
		},
		idlTargetScala := {
			(Compile / sourceManaged).value / "pidl"
		},
		idlTargetScript := {
			(Compile / resourceManaged).value / "pidl"
		},

		// build the/a classpath that idl will scan
		idlClassPath := {
			Stream(
				(Compile / unmanagedClasspath).value,
				(Compile / managedClasspath).value,
				(Compile / externalDependencyClasspath).value,
				(Compile / internalDependencyClasspath).value,
			)
				// map the record thing
				.flatMap(_.toStream.map(_.data.getAbsoluteFile))
				// make it distinct
				.distinct
		},

		// add the .pidl stuff to the resources
		Compile / unmanagedResourceDirectories ++= idlRoots.value,

		//
		// define the tasks
		//

		// scala task
		Compile / sourceGenerators += idlGenerateScala,
		idlGenerateScala := {
			val pak = "S3" // organization.value + "." + name.value
			val src = idlRoots.value
			val out = idlTargetScala.value
			val inc = idlClassPath.value

			S3.Scala(pak, src, inc, out)
		},

		// purescript generation tasks
		Compile / resourceGenerators += idlGenerateScript,
		idlGenerateScript := {
			val pak = "S3" // organization.value + "." + name.value
			val src = idlRoots.value
			val out = idlTargetScript.value
			val inc = idlClassPath.value

			S3.Script(pak, src, inc, out)
		},
	)
}
