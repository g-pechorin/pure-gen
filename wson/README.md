

This module provides (some) Automatic Speech Functionality.

I was originally written for IBM's Watson ASR, but, that didn't work for me.

It now uses;

- CMUSphinx4; an on-chip pure-Java system
- Google's ASR; more-accurate but cloud based
    - this relies upon some credentials
