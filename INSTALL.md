

The project operates on amd64 based Windows, Linux and MacOS computers - this is a limitation of the [Spago] and [PureScript] toolchains.
You will need git<a id='f_link2' name='f_link2'/><sup>[2](#f_note2)</sup> and [JDK11+](https://adoptopenjdk.net/) installed and on the command line.<a id='f_link1' name='f_link1'/><sup>[1](#f_note1)</sup>
You can check with the following commands;

```bash
$ git --version
$ javac --version
$ java --version
```

The git version is probably irrelevant but Java has to be 11 or later because Oracle's GraalJS engine needs that internally.<a id='f_link1' name='f_link1'/><sup>[1](#f_note1)</sup>
With that in place you can clone (et al) the project and "test" it to be sure it's all running;

```bash
$ git clone -b default https://gitlab.com/g-pechorin/pure-gen.git
$ cd pure-gen
$ java -jar sbt.bin/sbt-launch.jar test

$ java -jar sbt.bin/sbt-launch.jar demo/run
```

The "agent window" should  (after a wall of text) appear like this;

![running-agent](doc/control-tick.png)

Speaking (simple usually monosyllabic) words like "no" and "hi" should be quickly recognised allowing the system to repeat them back using the speech synthesizer.

I have tested this on;

- Windows 7 (my Desktop - most recent)
- Windows 10 (work laptop - less recent)
- Ubuntu ??? (personal Laptop - previously)
- an old Mac Mini (occasionally)<a id='f_link3' name='f_link3'/><sup>[3](#f_note3)</sup>

It requires an AMD64 CPU (because of the PureScript compiler) but I'd be interested to know if x86 JDK versions "work".
While there is a curent mode to "build" on an AMD64 desktop and "deploy" to something like a RaspberryPi - it's not been a priority for me.

[When you're ready - why not continue with the tutorial?](TUTORIAL.md)

----

<a id='f_note1' name='f_note1'/><b>[1](#f_link1)</b>
I tried to minimise "hard" requirements.
JDK11 and git seemed minimal for this sort of project.
[back](#f_link1)

<a id='f_note2' name='f_note2'/><b>[2](#f_link2)</b>
Spago needs git installed - not me!
[back](#f_link2)

<a id='f_note3' name='f_note3'/><b>[3](#f_link3)</b>
[OpenJDK is available for macOS](https://adoptopenjdk.net/releases.html) and runs just fine.
The device frequently reboots which makes it a challenge to use as a home server or Docker host.
I've found the macOS unsuitable for "workstation" stuff - the keyboard/mouse shortcuts are just weird, and, the reboots seem to reset my keyboard language.
[back](#f_link3)
