
/// ====
// monorepo config block

import java.io.File

val hgRoot: File = {
	var root = file("").getAbsoluteFile

	while (!(root / "sbt.bin/scala.conf").exists())
		root = root.getAbsoluteFile.getParentFile.getAbsoluteFile

	root
}

def conf: String => String = {
	import com.typesafe.config.ConfigFactory

	(key: String) =>
		ConfigFactory.parseFile(
			hgRoot / "sbt.bin/scala.conf"
		).getString(key)
}
// end of monorepo config block

organization := "com.peterlavalle"
scalaVersion := conf("scala.version")
scalacOptions ++= conf("scala.options").split("/").toSeq

resolvers += Classpaths.typesafeReleases
resolvers += Resolver.mavenCentral
resolvers += Resolver.jcenterRepo
resolvers += "jitpack" at "https://jitpack.io"

// end of standard stuff
/// ---

name := "pure-gen"

//
// deinfe root and describe modules
lazy val root = {
	(project in file("."))
		.settings(
			name := "pureGen"
		)
		.settings(
			all: _ *
		)
		.aggregate(

			// shared; this is the core functionality shared by all aspects of the system
			core,

			// endpoint; this is the parrot example
			demo,

			// module; this is the MaryTTS copmponent
			mary,

			// tooling; this is some of the spago logic - i think that the rest is external in PureSand
			spgo,

			// tooling; this module provides the GUI and CLI logic used by the demo
			tool,

			// module; this (started as IBM's Watson ASR and now) provides the Google and CMUSphinx4 ASR systems
			wson,
		)
}

//
// this bit of weirdness fixes an XSLT error in the CLI and IDEA
// this might need to be "inverted" for the/a
def maryXSLTFix = {
	// is this in the IntelliJ IDEA? ...
	if ("true" == System.getProperty("idea.managed"))
	// ... then addind the stuff below causes an error
		Seq()
	else
	// ... we're using the CLI and need these to "fix" and XSLT transformer error
		Seq(
			// https://github.com/marytts/marytts/issues/455
			"xalan" % "xalan" % "2.7.2",

			// https://github.com/marytts/marytts/issues/740
			"net.sf.saxon" % "Saxon-HE" % "9.7.0-18",
		)
}

lazy val all: Seq[Def.Setting[_]] =
	Seq(
		scalaVersion := conf("scala.version"),
		resolvers += Classpaths.typesafeReleases,
		resolvers += Resolver.mavenCentral,
		resolvers += Resolver.jcenterRepo,
		resolvers += "jitpack" at "https://jitpack.io",

		// needed for marytts
		resolvers += Resolver.sonatypeRepo("public"),
		resolvers += Resolver.sonatypeRepo("snapshots"),
		resolvers += Resolver.sonatypeRepo("staging"),
		resolvers += Resolver.sonatypeRepo("releases"),

		// here's hoping that this'll fix that problem with scala paths and linux
		scalacOptions ++= Seq("-Xmax-classfile-name", "128"),
	)

lazy val core = {
	project
		.settings(all: _ *)
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "minibase" % "a7dde6f")
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "minitest" % "c4a37cf" % Test)
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "jopcyaon" % "0c29c14")
		.dependsOn(tool)
		.settings(
			libraryDependencies += "org.graalvm.js" % "js" % conf("graaljs"),
			Compile / unmanagedResourceDirectories +=
				new File(baseDirectory.value.getParentFile, "lib")
					.getAbsoluteFile,
		)
}

lazy val demo = {
	project
		.enablePlugins(pal.PIDLPlugin)
		.settings(all: _ *)
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "minibase" % "a7dde6f")
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "minitest" % "c4a37cf" % Test)
		.dependsOn(
			core,
			mary,
			spgo,
			wson,
		)
}

lazy val mary = {
	project
		.settings(all: _ *)
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "minibase" % "a7dde6f")
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "minitest" % "c4a37cf" % Test)
		.dependsOn(core)
		.settings(

			libraryDependencies ++= maryXSLTFix,

			libraryDependencies ++= Seq(
				"de.dfki.mary" % "voice-cmu-slt-hsmm" % conf("marytts"),
				"de.dfki.mary" % "marytts-client" % conf("marytts"),
				"de.dfki.mary" % "marytts-common" % conf("marytts"),
			)
		)
}

lazy val spgo = {
	project
		.settings(all: _ *)
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "minibase" % "a7dde6f")
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "minitest" % "c4a37cf" % Test)
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "puresand" % "30d267f")
		.dependsOn(core)
		.settings(
			libraryDependencies += "org.graalvm.js" % "js" % conf("graaljs"),
			Compile / resourceDirectory := (Compile / scalaSource).value,
			Test / resourceDirectory := (Test / scalaSource).value,
		)
}

lazy val tool = {
	project
		.settings(all: _ *)
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "minibase" % "a7dde6f")
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "minitest" % "c4a37cf" % Test)
}

lazy val wson = {
	project
		.settings(all: _ *)
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "minibase" % "a7dde6f")
	.settings(libraryDependencies += "com.gitlab.g-pechorin" % "minitest" % "c4a37cf" % Test)
		.dependsOn(core)
		.settings(
			libraryDependencies ++= Seq(
				// "com.ibm.watson" % "speech-to-text" % "8.5.0",

				"com.google.cloud" % "google-cloud-speech" % "1.24.0",
				"commons-cli" % "commons-cli" % "1.4" % Test,

				// "org.mozilla.deepspeech" % "libdeepspeech" % "0.8.1",
				// // "org.mozilla.deepspeech" % "libdeepspeech" % "0.7.4",

				// show sphinx doing the thing
				"edu.cmu.sphinx" % "sphinx4-core" % "5prealpha-SNAPSHOT",
				"edu.cmu.sphinx" % "sphinx4-data" % "5prealpha-SNAPSHOT",
			)
		)
}
