package peterlavalle.puregen

object ToolDemo {
	def main(args: Array[String]): Unit = {
		Tool("tool", null)
			.execute(
				new StartRunClose {
					override def run(): Unit = println("push a cycle")

					override def close(): Unit = println("okay close now")

					override def start(): Unit = println("up we go")
				}
			)
	}
}
