package peterlavalle.puregen

/**
 * this trait is just generically used here but nowhere else
 */
trait StartRunClose extends Runnable with AutoCloseable {
	def start(): Unit
}

