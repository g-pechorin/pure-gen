package peterlavalle.puregen

import java.awt._
import java.awt.event.{ActionEvent, ComponentAdapter, ComponentEvent}
import java.io.File
import java.net.URL
import java.util

import javax.swing._
import peterlavalle.PiSwingT.{SwingNot, SwingNow, SwingVal}
import peterlavalle._

trait Tool extends Tool.Request {
	/**
	 * execute a SRC. it's unclear if this should return ASAP or only when done.
	 *
	 * i was expecting "when done" but the current GUI does "ASAP"
	 *
	 * @param code
	 */
	def execute(code: StartRunClose): Unit
}

object Tool {
	/**
	 * an expression to open  something in Code
	 */
	val openCode: File => Unit = {

		lazy val cmd: File => Seq[String] = {
			root: File =>

				lazy val E(code: File) =
					onPath("code")

				Seq(code.getAbsolutePath, root.getAbsolutePath)
		}

		root: File =>
			require {
				import sys.process._
				0 == (cmd(root) !)
			}
	}

	trait Signal[T] extends (T => Unit)

	trait Request {
		def log(name: String): Signal[String]

		lazy val delegate: Request = {
			case class Delegate(from: Request) extends Request {
				override def log(name: String): Signal[String] = from.log(name)
			}

			this match {
				case Delegate(_) => this
				case _ => Delegate(this)
			}
		}
	}

	def apply(title: String, root: File, size: Int = 16): Tool = new Tool {

		object Logs {
			val logs = new util.LinkedList[(String, String)]()

			val textArea = new JTextArea()

			textArea.setName("Logs")
			textArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14))

			def update(): Unit =
				SwingVal {
					textArea.setText(
						logs
							.map {
								case (name, text) =>
									text.split("[\r \t]*\n")
										.foldLeft(name)(_ + "\n\t" + _)
							}
							.foldLeft("")(_ + _ + "\n")
					)
				}
		}

		override def log(name: String): Signal[String] = {

			require(!Logs.logs.exists(_._1 == name))

			val index = Logs.logs.size()

			Logs.logs.addLast(name -> "")

			(line: String) =>
				Logs.logs.set(
					index,
					name -> line
				)
				Logs.update()
		}

		override def execute(startRunClose: StartRunClose): Unit = {
			lazy val frame = new JFrame(title)
			SwingNow {
				frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)
				frame.setLayout(new BorderLayout())

				//
				// add the toolbar at the top
				frame.add(
					tooBar(
						urlIconButton("force cycle", size, "https://static.thenounproject.com/png/2069177-200.png") {
							SwingNot {
								startRunClose.run()
							}
						},
						urlIconButton("close system", size, "https://static.thenounproject.com/png/55049-200.png") {
							SwingNow {
								SwingNot {
									startRunClose.close()
								}
								frame.dispose()
							}
						},
						// FIXME
						urlIconButton("edit in Code", size, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTOQw3ItXhKpNkfcQn3YXCXQOoA-Xp0UcDy1g&usqp=CAU") {
							SwingNow {
								SwingNot {
									openCode(root)
								}
							}
						},

					),
					BorderLayout.NORTH
				)

				//
				// create the central jtabbed pane
				val tabs = new JTabbedPane()

				tabs.add("text log columns", new JScrollPane(Logs.textArea))

				//
				// add the tabbed stuff
				frame.add(tabs, BorderLayout.CENTER)

				frame.addComponentListener(
					new ComponentAdapter {
						override def componentShown(componentEvent: ComponentEvent): Unit = {
							frame.removeComponentListener(this)
							startRunClose.start()
						}
					}
				)
				frame.setPreferredSize(new Dimension(640, 480))
				frame.setVisible(true)
				frame.pack()
			}

			while (frame.isVisible)
				Thread.sleep(1000)
		}
	}

	def urlIconButton(text: String, size: Int, url: String)(action: => Unit): JButton = {
		val imageIcon = new ImageIcon(new URL(url))

		val scaled = imageIcon.getImage.getScaledInstance(size, size, Image.SCALE_SMOOTH)

		imageIcon setImage scaled

		val button = new JButton(text)

		button setIcon imageIcon

		button.addActionListener((_: ActionEvent) => action)

		button
	}

	def tooBar(components: Component*): JToolBar = {
		val jToolBar = new JToolBar()
		jToolBar.setFloatable(false)
		components.foreach(jToolBar.add(_: Component))
		jToolBar
	}

}
