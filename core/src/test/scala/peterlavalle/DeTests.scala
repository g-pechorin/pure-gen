package peterlavalle

import org.scalatest.funsuite.AnyFunSuite

class DeTests extends AnyFunSuite {

	case class Foo(int: Int)

	case class Bar(foo: Foo, seq: Stream[Float])

	trait deBase[T] extends De[T] {
		implicit val deScriptDouble: DeScript[Double]
		implicit val deScriptString: DeScript[String]
		implicit val deScriptInt: DeScript[Int]
		implicit val deScriptBoolean: DeScript[Boolean]
		implicit val deScriptLong: DeScript[Long]
		implicit val deScriptFloat: DeScript[Float]
		implicit val deScriptByte: DeScript[Byte]
		implicit val deScriptShort: DeScript[Short]
	}

	trait deFoo[T] extends
		deBase[T] with
		De[T] {

		implicit val deScriptFoo: DeScript[Foo] =
			for {
				int <- "int".field[Int]
			} yield {
				Foo(int)
			}

	}

	trait deBar[T] extends
		deBase[T] with
		deFoo[T] with
		De[T] {

		implicit val deScriptBar: DeScript[Bar] =
			for {
				seq <- "seq".stream[Float]
				foo <- "foo".field[Foo]
			} yield {
				Bar(foo, seq)
			}
	}

	/**
	 * this small test shouldn't run anything, but, it should "prove" that the De[T] type can be used for "inverted trait"
	 */
	test("prove that De[T] is more generic than I realised") {
		val lang =
			new deBar[String] with deFoo[String] {
				override implicit lazy val deScriptDouble: DeScript[Double] = ???
				override implicit lazy val deScriptString: DeScript[String] = ???
				override implicit lazy val deScriptInt: DeScript[Int] = null
				override implicit lazy val deScriptBoolean: DeScript[Boolean] = ???
				override implicit lazy val deScriptLong: DeScript[Long] = ???
				override implicit lazy val deScriptFloat: DeScript[Float] = null
				override implicit lazy val deScriptByte: DeScript[Byte] = ???
				override implicit lazy val deScriptShort: DeScript[Short] = ???
				override lazy val get: (String, String) => String = ???
				override lazy val seq: String => Iterable[String] = ???
			}

		if (false)
			lang.deScriptBar.parse(???)
	}

}
