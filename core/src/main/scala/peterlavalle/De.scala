package peterlavalle

import scala.language.higherKinds

/**
 * trait to handle conversions between like-JavaScript and Scala
 *
 * currently used to convert GraalJS ... and needs to be merged with an org.json approach
 *
 * @tparam V the Value/Object thing. Here it's going to be Graal script
 */
trait De[V] {

	val get: (V, String) => V
	val seq: V => Iterable[V]

	trait DeScript[T] {
		def parse(value: V): T
	}

	/**
	 * silly trait to handle mixing parser with other things
	 */
	trait DeScripter[T] extends DeScript[T] {
		val parser: DeScript[T]

		override final def parse(value: V): T = parser.parse(value)
	}

	implicit class DeValue(value: V) {
		// TODO; can I make it *just* this one?
		def toValue[T: DeScript]: T = implicitly[DeScript[T]].parse(value)
	}

	implicit class DeField(key: String) {
		def field[T: DeScript]: TPureBind[T, DeScript] =
			new TPureBind[T, DeScript] {
				val deScript = implicitly[DeScript[T]]

				override def map[O](f: T => O): DeScript[O] =
					(value: V) =>
						f(deScript.parse(get(value, key)))

				override def flatMap[O](f: T => DeScript[O]): DeScript[O] =
					(value: V) =>
						f(deScript.parse(get(value, key))).parse(value)
			}

		def stream[T: DeScript]: TPureBind[Stream[T], DeScript] = {
			new TPureBind[Stream[T], DeScript] {
				val deScript = implicitly[DeScript[T]]

				override def map[O](f: Stream[T] => O): DeScript[O] =
					(value: V) =>
						f(seq(get(value, key))
							.toStream
							.map(deScript.parse(_: V)))

				override def flatMap[O](f: Stream[T] => DeScript[O]): DeScript[O] =
					(value: V) =>
						f(seq(get(value, key))
							.toStream
							.map(deScript.parse(_: V))).parse(value)

			}
		}
	}

	trait TPureBind[K, M[_]] {
		def map[O](f: K => O): M[O]

		def flatMap[O](f: K => M[O]): M[O]
	}

}

