package peterlavalle.puregen

import java.io.{File, FileWriter}

import org.graalvm.polyglot.Context
import peterlavalle.CLI

import scala.io.{BufferedSource, Source}

/**
 * provides an abstract class to extend and build a CLI from
 * @param root
 */
abstract class APureGenApp(root: File) extends CLI {

	final val main: cli =
		for {
			code <- "code" arg "" // this accepts a compiled agent without compiling it
			dump <- "dump" arg "" // this refers to dumping the/a compiled agent
			root <- "root" arg root
			path <- "path" arg "iai" // just the iai folder; the only place an agent should appear
			main <- "main" arg "Main"
		} yield {

			require(code.isEmpty || dump.isEmpty || (dump == code))

			// branch to either load previously compiled agent or compile one ourself
			// ... useful if we want to run on something like a Pi3 where PureScript does not run
			val built: String = {
				if (code.nonEmpty && dump.isEmpty) {
					val file: File = root / code

					require(
						file.exists(),
						"can't load from missing file " + file.AbsolutePath
					)
					require(
						file.isFile(),
						"can't load from non-file " + file.AbsolutePath
					)

					Source.fromFile(file)
						.using((_: BufferedSource).mkString)
				} else {
					compile(
						root,
						root / path,
						main
					)
				}
			}

			// if there's a dump parameter; dump the script
			if (dump.nonEmpty)
				new FileWriter((root / dump).EnsureParent)
					.append(built)
					.close()

			// if there's no dump or we're dumping and running; then run the agent
			if (dump.isEmpty || (dump == code))
				BuiltIn(

					built,
					guiTask(root)
				)(s3)
		}

	def s3(context: Context, pedal: BuiltIn.Hook, run: Runnable): Unit

	def guiTask(root: File): Tool = {
		Tool("name", root)
	}

	protected[this] def compile(root: File, agent: File, main: String): String
}
