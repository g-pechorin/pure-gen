package peterlavalle.puregen

import org.graalvm.polyglot.{Context, Value}
import peterlavalle.jopc.Dez

/**
 * it's a Dez but for Graal.JS values
 */
class DezValue(context: Context) extends Dez.All[Value] {

	trait Child extends super.Child
		with AltChild
		with AtomicChild


	override implicit val dezShort: De[Short] =
		deAtomic[Short](
			(z: Value, k: String, v: Short) => {
				z.putHashEntry(k, v)
				z
			}
		)(
			(z, k) =>
				z -> z.getHashValue(k).asShort()
		)

	override implicit val dezString: De[String] =
		deAtomic[String](
			(z: Value, k: String, v: String) => {
				z.putHashEntry(k, v)
				z
			}
		)(
			(z: Value, k: String) =>
				z -> z.getHashValue(k).asString()
		)

	override implicit val dezBoolean: De[Boolean] =
		deAtomic[Boolean](
			(z: Value, k: String, v: Boolean) => {
				z.putHashEntry(k, v)
				z
			}
		)(
			(z: Value, k: String) =>
				z -> z.getHashValue(k).asBoolean()
		)

	override implicit val dezInt: De[Int] =
		deAtomic[Int](
			(z: Value, k: String, v: Int) => {
				z.putHashEntry(k, v)
				z
			}
		)(
			(z: Value, k: String) =>
				z -> z.getHashValue(k).asInt()
		)

	override implicit val dezFloat: De[Float] =
		deAtomic[Float](
			(z: Value, k: String, v: Float) => {
				z.putHashEntry(k, v)
				z
			}
		)(
			(z: Value, k: String) =>
				z -> z.getHashValue(k).asFloat()
		)

	override implicit val dezChar: De[Char] =
		deAtomic[Char](
			(z: Value, k: String, v: Char) => {
				z.putHashEntry(k, s"$v")
				z
			}
		)(
			(z: Value, k: String) =>
				z.getHashValue(k).asString().toList match {
					case List(v) => z -> v
				}
		)

	override implicit val dezDouble: De[Double] =
		deAtomic[Double](
			(z: Value, k: String, v: Double) => {
				z.putHashEntry(k, v)
				z
			}
		)(
			(z: Value, k: String) =>
				z -> z.getHashValue(k).asDouble()
		)

	override implicit val dezByte: De[Byte] =
		deAtomic[Byte](
			(z: Value, k: String, v: Byte) => {
				z.putHashEntry(k, v)
				z
			}
		)(
			(z: Value, k: String) =>
				z -> z.getHashValue(k).asByte()
		)

	override implicit val dezLong: De[Long] =
		deAtomic[Long](
			(z: Value, k: String, v: Long) => {
				z.putHashEntry(k, v)
				z
			}
		)(
			(z: Value, k: String) =>
				z -> z.getHashValue(k).asLong()
		)

	override def put(z: Value, k: String, v: Seq[Value]): Value = {

		// start an array
		val arr: Value = context.eval("js", "[]")

		// add all values
		v.foreach((v: Value) => arr.invokeMember("push", v))

		// update the value
		z.putHashEntry(k, arr)

		// we return the same thing though
		z
	}


	/**
	 * dez: here's another secret - sequences are treated really different AND can only be fat-records
	 */
	override protected def seq(z: Value, k: String): (Value, Seq[Value]) = ???

	/**
	 * test if a container is "at end"
	 *
	 * used to verify that fields are decomposed properly - for Random and JSON just return true always
	 *
	 * ... i suppose we could erase JSON values as we read them ... which would do a nice checking. but it's bad because it would prevent wrong JSON ... aah ... wait; i guess that i shold do that.
	 */
	override protected def eoz(z: Value): Boolean = true

	override protected def newZInstance(): Value =
		context.eval("js", "{}")
}
