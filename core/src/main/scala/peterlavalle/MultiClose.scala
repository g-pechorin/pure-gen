package peterlavalle

import java.util

/**
 * mixin trait to add thing(s) that're going to close other things.
 *
 * thread safe. only allows a single closing.
 */
trait MultiClose extends AutoCloseable {

	/**
	 * collection of things to do when we "close"
	 */
	private val closeActions: util.HashSet[AutoCloseable] = {
		val closeActions = new util.HashSet[AutoCloseable]()
		closeActions.add(() => ()) // dumb-hack; the "unclosed" collection will not be empty
		closeActions
	}

	class PreviouslyClosedException private[MultiClose]() extends Exception

	override final def close(): Unit =
		closeActions.synchronized {

			// can't close a second time
			if (closeActions.isEmpty)
				throw new PreviouslyClosedException()

			// pull the data out of the set - then - execute all actions
			val copy: Set[AutoCloseable] = closeActions.toSet
			closeActions.clear()
			copy.foreach((_: AutoCloseable).close())
		}

	/**
	 * schedule an action for close
	 */
	protected final def atClose(action: => Unit): Unit =
		atClose(
			() => action
		)

	/**
	 * mark something as needing to be closed
	 */
	protected final def atClose(action: AutoCloseable): Unit =
		closeActions.synchronized {

			// can't close a second time
			if (closeActions.isEmpty)
				throw new PreviouslyClosedException()

			closeActions.add(action)
		}
}
