package peterlavalle.pybx

import java.io.{FileInputStream, OutputStream}

class PocketSphinxTest extends APyBedTest {

	import PyBed._

	testPy("hello world - but with pocketsphinx imported", "pocketsphinx")(
		code = _ =>
			py {
				(port: Int) =>
					s"""
						 |import pocketsphinx
						 |s.sendall(b'hello world - but with pocketsphinx')
					""".stripMargin.trim
			},
		body = _ => {},
		done = (output: Array[Byte]) => {

			val actual: String =
				new String(output, "UTF-8")

			assert(actual == "hello world - but with pocketsphinx")
		}
	)

	testPy("dir pocketsphinx", "pocketsphinx")(
		code =
			_ =>
				py {
					(port: Int) =>
						s"""
							 |import pocketsphinx
							 |for c in dir(pocketsphinx):
							 |	s.sendall((c + '\\n').encode('utf-8'))
					""".stripMargin.trim
				},
		body = _ => {},
		done = (output: Array[Byte]) => {

			val actual =
				new String(output, "UTF-8")
					.split("[\r \t]*\n")
					.toSet

			val wanted =
				Set(
					"AudioFile",
					"Config",
					"Decoder",
					"DefaultConfig",
					"Feature",
					"FrontEnd",
					"FsgModel",
					"Hypothesis",
					"Lattice",
					"LiveSpeech",
					"LogMath",
					"Pocketsphinx",
					"Segment",
					"SegmentIterator",
					"SegmentList",
					"pocketsphinx",
					"signal",
					"sphinxbase",
				)

			assert(
				wanted
					.filterNot(actual) == Set()
			)
		}
	)

	val raw =
		List(
			"goforward.raw" -> "go forward ten meters",
			"numbers.raw" -> "thirty three four or six ninety two",
			"something.raw" -> "go somewhere and do something",
		)

	lazy val data = {
		val data = "pybx" / "test-data"

		require(data.isDirectory)

		require(
			raw.forall(data / _._1 isFile)
		)

		data
	}

	testPy("really test pocketsphinx", "pocketsphinx")(

		// use the/my main approach
		code = TinySphinx(_),

		// dump the raw files into the socket
		body = (out: OutputStream) =>{
			raw
				.map(data / _._1)
				.map(new FileInputStream(_))
				.foreach {
					seg =>
						out << seg flush()
						Thread.sleep(3140)
				}
		},

		// check/test that we got the expected spoken strings out
		done = (output: Array[Byte]) =>
			assert {
				new String(output, "UTF-8")
					.split("[\r \t]*\n")
					.toList == raw.map(_._2)
			}
	)
}
