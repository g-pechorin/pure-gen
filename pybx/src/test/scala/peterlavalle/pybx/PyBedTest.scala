package peterlavalle.pybx

import java.io.{ByteArrayOutputStream, File, OutputStream}

import peterlavalle._

class PyBedTest extends APyBedTest {

	/**
	 * create a thing that sends a "hello world" out
	 */
	test("hello world") {

		// run the program and collect the output
		val output: Array[Byte] =
			TestTemp {
				// get a temporary directory
				dir: File =>

					// buffer output
					new ByteArrayOutputStream()
						.using {
							stream: ByteArrayOutputStream =>
								pyBed.open(
									dir,
									(port: Int) =>
										s"""
											 |#!/usr/bin/env python3
											 |
											 |import socket
											 |
											 |with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
											 |    s.connect(('127.0.0.1', $port))
											 |    print('client has the socket')
											 |    s.sendall(b'hello world')
											 |print('client has completed')
										""".stripMargin.trim,
									Seq(),
									stream
								).using {
									_: OutputStream =>
										// need to wait for the python thing/stuff to "spin up"
										while (stream.toByteArray.isEmpty) {
											Thread.sleep(3140)
										}
										println("test is done waiting")
								}
								stream.toByteArray
						}
			}

		val actual: String =
			new String(output, "UTF-8")

		assert(actual == "hello world")
	}

	testPy("helly world - py")(
		code = _ =>(port: Int) =>
			s"""
				 |#!/usr/bin/env python3
				 |
				 |import socket
				 |
				 |with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
				 |    s.connect(('127.0.0.1', $port))
				 |    print('client has the socket')
				 |    s.sendall(b'hello world')
				 |print('client has completed')
										""".stripMargin.trim,
		body = _ => {},
		done = (output: Array[Byte]) => {

			val actual: String =
				new String(output, "UTF-8")

			assert(actual == "hello world")
		}
	)

	/**
	 * same test, but, we use a dirty directory and repeat it
	 */
	test("hello world (2) reuse") {
		TestTemp {
			// get a temporary directory
			dir: File =>
				(0 until 4)
					.foreach {
						idx: Int =>

							// run the program and collect the output
							val output: Array[Byte] =
								new ByteArrayOutputStream()
									.using {
										stream: ByteArrayOutputStream =>
											pyBed.open(
												dir,
												port =>
													s"""
														 |#!/usr/bin/env python3
														 |
														 |import socket
														 |
														 |with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
														 |    s.connect(('127.0.0.1', $port))
														 |    print('client$idx has the socket')
														 |    s.sendall(b'hello world ($idx)')
														 |print('client$idx has completed')
													""".stripMargin.trim,
												Seq(),
												stream
											).using {
												_: OutputStream =>
													// need to wait for the python thing/stuff to "spin up"
													while (stream.toByteArray.isEmpty) {
														Thread.sleep(3140)
													}
													println(s"test $idx is done waiting")
											}
											stream.toByteArray
									}


							val actual: String =
								new String(output, "UTF-8")

							assert(actual == s"hello world ($idx)")
					}
		}
	}

}
