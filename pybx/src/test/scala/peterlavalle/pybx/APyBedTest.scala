package peterlavalle.pybx

import java.io.{ByteArrayOutputStream, File, OutputStream}

import org.scalatest.funsuite.AnyFunSuite
import peterlavalle._

trait APyBedTest extends AnyFunSuite {

	val pyBed: PyBed = {
		osNameArch {
			case ("windows", _) =>
				PyBedDiagonal1
		}
	}

	def testPy(name: String, pip: String*)(code: File => Int => String, body: OutputStream => Unit, done: Array[Byte] => Unit) = {
		test(name) {
			// run the program and collect the output
			done {
				TestTemp {
					// get a temporary directory
					dir: File =>
						// buffer output
						new ByteArrayOutputStream()
							.using {
								stream: ByteArrayOutputStream =>
									pyBed.open(
										dir,
										code(dir),
										pip,
										stream
									).using {
										out: OutputStream =>

											body(out)

											// need to wait for the python thing/stuff to "spin up"
											while (stream.toByteArray.isEmpty) {
												Thread.sleep(3140)
											}
									}
									stream.toByteArray
							}
				}
			}
		}
	}

}
