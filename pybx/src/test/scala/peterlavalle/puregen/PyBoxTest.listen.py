#!/usr/bin/env python

"""
run with <model-dir> <host> <port>
"""

from os import  path
from sys import argv
from pocketsphinx import Decoder

# configure the reader using data in a known dir
MODELDIR = '<@work/>'
config = Decoder.default_config()
config.set_string('-hmm', path.join(MODELDIR, 'en-us/en-us'))
config.set_string('-lm', path.join(MODELDIR, 'en-us/en-us.lm.bin'))
config.set_string('-dict', path.join(MODELDIR, 'en-us/cmudict-en-us.dict'))

## log meee!
#config.set_string('-logfn', 'NUL')

# create the ASR
decoder = Decoder(config)

# create the socket
from socket import socket, AF_INET,SOCK_STREAM
socket = socket(AF_INET, SOCK_STREAM)
socket.connect(('<@host/>', <@port/>))

# loop forever!
in_speech_bf = False
decoder.start_utt()
while True:
	buf = socket.recv(<@size/>)
	if buf:
		decoder.process_raw(buf, False, False)
		if decoder.get_in_speech() != in_speech_bf:
			in_speech_bf = decoder.get_in_speech()
			if not in_speech_bf:
				decoder.end_utt()
				result = decoder.hyp()
				<@line/>
				decoder.start_utt()
	else:
		break
decoder.end_utt()
