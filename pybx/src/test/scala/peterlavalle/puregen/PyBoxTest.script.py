#!/usr/bin/env python

from os import environ, path

from pocketsphinx import Decoder

MODELDIR = 'target/model'
DATADIR = 'target/test.data'

DATADIR = './'
MODELDIR = 'model'

config = Decoder.default_config()

config.set_string('-hmm', path.join(MODELDIR, 'en-us/en-us'))
config.set_string('-lm', path.join(MODELDIR, 'en-us/en-us.lm.bin'))
config.set_string('-dict', path.join(MODELDIR, 'en-us/cmudict-en-us.dict'))

# best to just leave this as is and pattern match
# config.set_string('-logfn', 'NUL')

decoder = Decoder(config)

in_speech_bf = False
decoder.start_utt()
for file in [
		'<@raw/>',
	]:
	stream = open(path.join(DATADIR, file), 'rb')
	while True:
		buf = stream.read(1024)
		if buf:
			decoder.process_raw(buf, False, False)
			if decoder.get_in_speech() != in_speech_bf:
				in_speech_bf = decoder.get_in_speech()
				if not in_speech_bf:
					decoder.end_utt()
					print ('Result:', decoder.hyp().hypstr)
					decoder.start_utt()
		else:
			break
decoder.end_utt()
