package peterlavalle.puregen

import java.io.{File, FileInputStream, FileOutputStream, OutputStream}
import java.net.URL
import java.util.zip.{ZipEntry, ZipInputStream}

import peterlavalle._

import scala.annotation.tailrec

trait PyBox {

	def py3(code: String): PyBox

	def py3(code: Iterable[String]): PyBox = py3(code.foldLeft("#!/usr/bin/env python3\n\n")(_ + _ + "\n"))

	def run(out: String => Unit, err: String => Unit): Int

	def run(): Int =
		run(
			System.out.println,
			System.err.println
		)
}

object PyBox {


	val hash = "ab6d6471800966990e12fdb6ed27ae36323cf2c4"
	val rModelContent = {
		require(hash matches "\\w{40}")
		("pocketsphinx-" + PyBox.hash + "/(model/en-us/.*)").r
	}
	val py3vEnv =
		osNameArch {
			case ("windows", _) => "python -m venv ./"
			case ("linux" | "mac", _) => "python3 -m venv ./"
		}

	def apply(work: File)(pips: String*): PyBox = {


		???
	}

	def dump(file: File, raw: Boolean = false): Unit = {
		error("do this shell as a thing")
		implicit class extension(zip: ZipInputStream) {
			@tailrec
			final def dump(into: String => OutputStream): Unit = {
				zip.getNextEntry match {
					case null => 0
					case entry: ZipEntry if entry.isDirectory =>
						dump(into)
					case entry: ZipEntry =>
						val stream = into(entry.getName)

						if (null == stream)
							dump(into)
						else {
							stream
								.write(
									zip.readNBytes(entry.getSize.toInt)
								)
							stream.close()
							dump(into)
						}
				}
			}
		}

		zipInputStream
			.dump {
				case PyBox.rModelContent(name) =>
					new FileOutputStream(file / name EnsureParent)

				case name if raw && name.endsWith(".raw") =>
					new FileOutputStream(file / name.reverse.takeWhile('/' != _).reverse EnsureParent)

				case _ =>
					null
			}
	}

	def apply(work: File, packages: String*): StepScript.Steps[StepScript] = {
		error("do this shell as a thing")

		// setup the project
		if (!(work / "Scripts/activate" isFile)) {
			StepScript(work)
				.step(py3vEnv)
				.exec()
		}

		TODO("check python version")

		// install packages
		packages
			.map("pip install " + _)
			.foldLeft(
				StepScript(work, "Scripts/activate")
			)(_ step _) match {
			case setup: StepScript =>
				setup.exec()
		}

		// now return our thing
		StepScript(work, "Scripts/activate")
	}

	@tailrec
	def zipInputStream: ZipInputStream = {


		val zipFile = "target" / s"$hash.zip"

		if (zipFile.exists())
			new ZipInputStream(new FileInputStream(zipFile))
		else {
			new FileOutputStream(zipFile)
				.using {
					out: FileOutputStream =>
						System.err.println("started downloading model/repo")
						out
							.write(
								new URL("https://codeload.github.com/cmusphinx/pocketsphinx/zip/" + hash)
									.openStream()
									.readAllBytes()
							)
						System.err.println("finished downloading model/repo")
				}
			zipInputStream
		}
	}
}
