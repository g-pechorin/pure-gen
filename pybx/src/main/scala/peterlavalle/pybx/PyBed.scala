package peterlavalle.pybx

import java.io.{File, OutputStream}
import java.util.Random

import scala.sys.process.ProcessLogger

/**
 * provides a way to run a sandboxed python
 *
 * my bitter attempt to normalise python execution.
 * this time i'm producing a wrapper in Scala-land with the intent to do os specific implementations.
 */
trait PyBed {

	/**
	 * the "real" version that the implementation must provide
	 */
	def open(
						// folder to work in
						dir: File,

						// the command to run with python-c given the port number
						run: Int => String,

						// pip packages to install
						pip: Seq[String],

						// source files to write
						src: Int => Map[String, String],

						// where to send the stdout and stderr
						log: ProcessLogger = ProcessLogger(System.out.println, System.err.println),

						// where to send whatever that is written to the socket
						out: OutputStream,

						// should we create any threads as a daemon
						isDaemon: Boolean = true,

						// create a random port
						portGen: () => Int = () => new Random().nextInt(2020) + 1024
					): OutputStream

	/**
	 * the "lazy" version that I'm probably going to end up using
	 */
	def open(
						// folder to work in
						dir: File,

						// the script to run - will be named after the call-stack-hash
						src: Int => String,

						// pip packages to install
						pip: Seq[String],

						// where to send whatever that is written to the socket
						out: OutputStream,

					): OutputStream = {

		val md5: String = {

			// the trace, minus "us"
			val trace: List[StackTraceElement] =
				Thread.currentThread()
					.getStackTrace
					.toList.tail
					.dropWhile(_.getClassName.endsWith(".PyBed"))

			// simple name to make life easier
			val name: String =
				trace
					.head.getClassName
					.reverse.dropWhile('$' == _).takeWhile('.' != _).reverse

			// we can do the/a "to string" because it's a list ... but i choose safety
			val hash =
				trace
					.foldLeft("")((_: String) + (_: StackTraceElement))
					.md5

			// computed value
			name + "-" + hash
		}

		open(
			if (null != dir)
				dir
			else
				(dir / "target" / md5).EnsureMkDirs,
			port => s"$md5.py",
			pip,
			port => Map(s"$md5.py" -> src(port)),
			ProcessLogger(System.out.println, System.err.println),
			out
		)
	}


}

object PyBed {

	def python(code: Int => Seq[String] ): Int => String =
		py {
			port =>
				code(port).foldLeft("")(_ + _ + "\n")
		}

	def py(code: Int => String, s: String = "s"): Int => String =
		(port: Int) => {
			Stream(
				"#!/usr/bin/env python3",
				"",
				"import socket",
				"",
				s"with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as $s:",
				s"\t$s.connect(('127.0.0.1', $port))",
				""
			) ++
				code(port)
					.split("[\r \t]*\n")
					.map("\t" + _)
		}.foldLeft("")(_ + _ + "\n")
}
