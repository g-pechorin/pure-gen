

from os import path

from pocketsphinx import *
from sphinxbase import *


config = Decoder.default_config()

MODELDIR = "<@root/>"

config.set_string('-hmm', path.join(MODELDIR, 'en-us'))
config.set_string('-lm', path.join(MODELDIR, 'en-us.lm.bin'))
config.set_string('-dict', path.join(MODELDIR, 'cmudict-en-us.dict'))

# log meee!
config.set_string('-logfn', 'NUL')



decoder = Decoder(config)

in_speech_bf = False
decoder.start_utt()
while True:
    buf = s.recv(1024)
    if buf:
        decoder.process_raw(buf, False, False)
        if decoder.get_in_speech() != in_speech_bf:
            in_speech_bf = decoder.get_in_speech()
            if not in_speech_bf:
                decoder.end_utt()
                hyp = decoder.hyp()
                print(dir(hyp))
                s.sendall((hyp.hypstr + '\n').encode('utf-8'))
                decoder.start_utt()
    else:
        break
decoder.end_utt()
