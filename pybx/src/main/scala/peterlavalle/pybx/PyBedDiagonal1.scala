package peterlavalle.pybx

import java.io.{File, FileWriter, OutputStream}
import java.net.{ServerSocket, Socket}

import scala.sys.process.{Process, ProcessLogger}

/**
 * for my workstation (which was named diagon for some reason)
 */
object PyBedDiagonal1 extends PyBed {
	/**
	 * the "real" version that the implementation must provide
	 */
	override def open(
										 dir: File,
										 cmd: Int => String,
										 pip: Seq[String],
										 src: Int => Map[String, String],
										 log: ProcessLogger,
										 out: OutputStream,
										 isDaemon: Boolean,
										 portGen: () => Int
									 ): OutputStream = {

		// choose a port
		val port = portGen()

		// create the TCP server
		val server = new ServerSocket(port)

		// create the python process as/in thread
		val process: Thread =
			new Thread() {

				setDaemon(isDaemon)
				start()

				override def run(): Unit = {

					log.out("dir = " + dir.AbsolutePath)

					// setup the env with pip
					require(0 == {
						Process(
							command = "python -m venv ./",
							cwd = dir.EnsureMkDirs
						) ! log
					})

					// update out pip
					log.out("pip = " + {
						Process(
							command = "CMD /C \"Scripts\\activate.bat && pip install --upgrade pip\"",
							cwd = dir.EnsureMkDirs
						) ! log
					})

					// check the pip details (because it keeps crashing)
					require(0 == {
						Process(
							command = "CMD /C \"Scripts\\activate.bat && pip -V\"",
							cwd = dir.EnsureMkDirs
						) ! log
					})

					// install our packages
					pip.foreach {
						pip =>
							require(0 == {
								Process(
									command = "CMD /C \"Scripts\\activate.bat && pip install --upgrade " + pip + "\"",
									cwd = dir.EnsureMkDirs
								) ! log
							})
					}

					// write our scripts
					src(port).foreach {
						case (name, code) =>
							new FileWriter(dir / name)
								.append(code)
								.close()
							log.out("wrote " + name)
					}

					// run the script
					require(0 == {
						Process(
							command = "CMD /C \"Scripts\\activate.bat && python " + (cmd(port)) + "\"",
							cwd = dir.EnsureMkDirs
						) ! log
					})
				}
			}

		// accept a connection
		val socket: Socket = {
			TODO("something magical to handle crashed python")
			server.accept()
		}

		// grab
		val output = socket.getOutputStream
		val input = socket.getInputStream

		// launch the pump-pipe thread
		val pump: Thread =
			new Thread() {
				override def run(): Unit = {

					val buffer = Array.ofDim[Byte](32)

					Stream
						.continually(input.read(buffer))
						.takeWhile(0 <= _)
						.foreach(out.write(buffer, 0, _))
					//					while (true) {
					//						val read = input.read(buffer)
					//						println(read)
					//						out.write(buffer, 0, read)
					//					}
				}

				setDaemon(isDaemon)
				start()
			}


		new OutputStream {
			override def write(i: Int): Unit = output.write(i)

			override def write(b: Array[Byte]): Unit = output.write(b)

			override def write(b: Array[Byte], off: Int, len: Int): Unit = output.write(b, off, len)

			override def flush(): Unit = output.flush()

			override def close(): Unit = {
				output.close()

				// close the output
				output.close()

				// close the socket
				socket.close()

				// await the thread(s)
				process.join()
				pump.join()

				// close the server
				server.close()
			}
		}
	}
}
