package peterlavalle.pybx

import java.io._
import java.util.zip.ZipInputStream

import peterlavalle.TemplateResource

object TinySphinx extends TemplateResource {

	def zipSequence: InputStream = {
		new SequenceInputStream(
			new java.util.Enumeration[InputStream] {

				val iterator =
					Stream
						.continually(())
						.zipWithIndex
						.map {
							case (_, idx) =>
								getClass.getResourceAsStream(
									"TinySphinx.model.zip." + (idx + 1).toString.reverse.padTo(3, '0').reverse
								)
						}
						.takeWhile(null != _)
						.iterator


				override def hasMoreElements: Boolean = iterator.hasNext

				override def nextElement(): InputStream = iterator.next()
			}
		)
	}

	def apply(root: File): Int => String = {

		require(root.EnsureMkDirs.isDirectory)

		new ZipInputStream(zipSequence)
			.using {
				zipFile =>
					Stream.continually(zipFile.getNextEntry)
						.takeWhile(null != _)
						.filterNot(_.isDirectory)
						.foreach {
							entry =>
								new FileOutputStream(root / entry.getName EnsureParent)
									.using(
										_.write(
											zipFile.readNBytes(entry.getSize.toInt)
										)
									)
						}
			}

		PyBed.python {
			(port: Int) =>
				bind("py") {
					case "root" =>
						root.AbsolutePath
				}
		}
	}

	def apply(bed: PyBed): File => OutputStream => OutputStream =
		(root: File) =>
			(out: OutputStream) =>
				bed.open(
					root,
					TinySphinx(root),
					Seq(
						"pocketsphinx",
					),
					out
				)
}
