package peterlavalle.puregen

import java.io.InputStream

import org.scalatest.funsuite.AnyFunSuite
import peterlavalle.{TAssertSourceEqual, TemplateResource}

import scala.io.{BufferedSource, Source}

class SpagoCodeTest extends AnyFunSuite with TAssertSourceEqual with TemplateResource {

	test("try it out") {
		assertSourceEqual(
			resource("try.json.txt"),
			SpagoCode
				.tasks(
					cache = "halbreich/grand",
					sep = "##"
				)

		)
	}

	protected def resource(res: String): Seq[String] = {
		val name: String = getClass.getSimpleName + "." + res
		val inputStream: InputStream = getClass.getResourceAsStream(name)
		require(null != inputStream, s"can't find resource `$name`")
		Source.fromInputStream(inputStream)
			.using((_: BufferedSource).mkString.split("[\r \t]*\n"))
	}

}
