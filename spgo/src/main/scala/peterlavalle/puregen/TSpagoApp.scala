package peterlavalle.puregen

import java.io.File

import peterlavalle.E

/**
 * mixin (based on spgo?) that lets APureGenApp compile
 */
trait TSpagoApp extends APureGenApp {

	override protected def compile(root: File, src: File, main: String): String = {

		// setup the visual code files
		SpagoCode(
			root, // where to set them up?
			"target/cache" // where to find spago and purs programs
		)

		val sandBox: File = root / "target/cache"
		val spago: File = root / "target/spago"

		//
		// setup our compiler
		val compile = new SpagoCompile(sandBox, spago)

		// compile the source
		compile.sources.add(src)

		// add our stuff like this
		compile.autoDependencies()

		// compile the agent
		compile
			.bundleModule(
				new SpagoCompile.Log {
					def flush(): Unit = {
						System.out.flush()
						System.err.flush()
					}

					override def out(line: Any): Unit = {
						flush()
						System.out.println(line)
					}

					override def err(line: Any): Unit = {
						flush()
						System.err.println(line)
					}
				},
				main
			)

			// trim
			.map((_: String).trim)
			.flatMap {
				str: String =>

					val ending: String = "module.exports = PS[\"" + main + "\"];"

					// validate that this ends with the "module" thingie
					if (!str.endsWith(ending))
						E ! "the source didn't end as expected"
					else {

						// right ... now rewrite the thing to allow me to pluck out the exported object
						E(
							(("(function(){" :: str.dropRight(ending.length).split("[\r \t]*\n").toList) :+ "return PS;})()")
								.foldLeft("")((_: String) + (_: String) + "\n")
						)
					}
			}
			.value
	}

}
