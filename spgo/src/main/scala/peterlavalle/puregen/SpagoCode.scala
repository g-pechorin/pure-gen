package peterlavalle.puregen

import java.io.{File, FileWriter, Writer}

import peterlavalle.TemplateResource


object SpagoCode extends TemplateResource {

	/**
	 * sets up the visual-code files
	 */
	def apply(into: File, cache: String): Unit = {

		// there's some hard-coded stuff here i'd like to move away from
		// println("vscode is " + into.ParentFile.AbsolutePath)

		Stream(
			"settings.json" -> settings(),

			// relative path
			// "tasks.json" -> tasks(cache),

			// absolute paths
			"tasks.json" -> tasks((into / cache).AbsolutePath),
		)
			.mapl(into.ParentFile / ".vscode/" / (_: String) EnsureParent)
			.mapl(new FileWriter(_: File): Writer)
			.foreach {
				case (file, data) =>
					data.foldLeft(file)((_: Writer) append (_: String) append "\n")
						.close()
			}
	}

	def settings(): Stream[String] =
		bind("settings.txt") {
			case null => ???
		}

	def tasks(
						 cache: String,
						 sep: String = File.pathSeparator
					 ): Stream[String] = {
		assume(cache contains '/')
		bind("tasks.txt") {
			case "sep" =>
				sep
			case "cache" =>
				cache
			case "parent" =>
				cache.reverse.dropWhile('/' != (_: Char)).tail.reverse
		}
	}
}
