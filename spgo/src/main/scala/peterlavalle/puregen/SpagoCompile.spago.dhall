{
	name = "my-project",
	dependencies = [
		<@dependencies/>
		"psci-support"
	],
	packages = ./packages.dhall,
	sources = [
		<@sources/>
	]
}
